# FreeCom

# Actively Developed

[https://github.com/FDOS/freecom](https://github.com/FDOS/freecom)


## Contributions

NLS specific corrections, updates and submissions should not be 
directly to submitted this project. NLS is maintained at the [FD-NLS](https://github.com/shidel/fd-nls)
project on GitHub. If the project is still actively maintained by it's
developer, it may be beneficial to also submit changes to them directly.

## FREECOM.LSM

<table>
<tr><td>title</td><td>FreeCom</td></tr>
<tr><td>version</td><td>0.85a</td></tr>
<tr><td>entered&nbsp;date</td><td>2021-07-10</td></tr>
<tr><td>description</td><td>The FreeDOS Command Shell</td></tr>
<tr><td>keywords</td><td>freecom freedos command shell</td></tr>
<tr><td>author</td><td>freedos-devel@lists.sourceforge.net (developers)</td></tr>
<tr><td>maintained&nbsp;by</td><td>freedos-devel@lists.sourceforge.net (maintainers)</td></tr>
<tr><td>primary&nbsp;site</td><td>https://github.com/FDOS/freecom</td></tr>
<tr><td>alternate&nbsp;site</td><td>http://freedos.sourceforge.net/</td></tr>
<tr><td>alternate&nbsp;site</td><td>http://www.freedos.org/</td></tr>
<tr><td>platforms</td><td>dos dosemu</td></tr>
<tr><td>copying&nbsp;policy</td><td>[GNU General Public License, Version 2 (GPL)](LICENSE)</td></tr>
<tr><td>wiki&nbsp;site</td><td>http://wiki.freedos.org/wiki/index.php/Command</td></tr>
</table>
